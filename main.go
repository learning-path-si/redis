package main

import (
    "log"
	"fmt"
	"flag"
	"strings"
	"github.com/joho/godotenv"
)

var mode string
var kNumOfRoutine int
var startNodes []string
var listName string
var qty int

// init is invoked before main()
func init() {
    // loads values from .env into the system
    if err := godotenv.Load(); err != nil {
        log.Print("No .env file found")
	}
	
	// кол-во потоков
	kNumOfRoutine = getEnvAsInt("NUM_ROUTINE", 0)
	// список узлов кластера
	startNodes = strings.Fields(getEnv("START_NODES", ""))
	// имя списка
	listName = getEnv("LIST_NAME", "")
	// кол-во элементов, обрабатываемых одним потоком
	qty = getEnvAsInt("QTY", 0)

	flag.StringVar(&mode, "mode", "help", "work mode: help, warm, read, loop")
}

func main() {
	flag.Parse()

	switch mode {
	case "help": fmt.Println("It's a place for help message.")
	case "read": read()
	case "warm": warm()
	case "loop": loop()
	default: 
			fmt.Printf("Something wrong: no %s mode!!!\n", mode)
	}
}


