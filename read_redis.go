package main

import (
    "log"
    "time"
	"fmt"
    "github.com/chasex/redis-go-cluster"
)

func read() {
    cluster, err := redis.NewCluster(
	&redis.Options{
	    StartNodes: startNodes,
	    ConnTimeout: 50 * time.Millisecond,
	    ReadTimeout: 50 * time.Millisecond,
	    WriteTimeout: 50 * time.Millisecond,
	    KeepAlive: 16,
	    AliveTime: 60 * time.Second,
	})

    if err != nil {
	log.Fatalf("redis.New error: %s", err.Error())
	}
	
    chann := make(chan int, kNumOfRoutine)
    for i := 0; i < kNumOfRoutine; i++ {
	go redisTest(cluster, i * qty, (i+1)*qty, chann)
    }

    for i := 0; i < kNumOfRoutine; i++ {
	_ = <-chann
    }
}

func redisTest(cluster *redis.Cluster, begin, end int, done chan int) {
	
    for i := begin; i < end; i++ {

		// Чтение из кластера
		value, err := cluster.Do("RPOP", listName)
		if err != nil {
			fmt.Printf("-error: %s\n", err.Error())
			time.Sleep(100 * time.Millisecond)
			continue
		}

		fmt.Printf("get %s\n", value)
		time.Sleep(500 * time.Millisecond)
    }

    done <- 1
}
