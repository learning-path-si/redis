# Load testing Redis

## Prerequisite

~~~~bash
go get github.com/joho/godotenv
go get github.com/chasex/redis-go-cluster
~~~~

## Build util

~~~~bash
make
~~~~

or (the same)

~~~~bash
go build -o bin/lt-redis-cli
~~~~

As result look for binary file lt-redis-cli in folder ./bin/ 

## Use util

In .env file you have to define some variables

NUM_ROUTINE     number of threads

LIST_NAME       name of the list in redis cluster

START_NODES     node list in redis cluster

QTY             quantities of list elements for one thread

Example

NUM_ROUTINE=5

LIST_NAME="mylist"

START_NODES="10.10.10.1:6379 10.10.10.2:6379 10.10.10.3:6379"

QTY=10

The following options are available:

 **-mode help** 

 **-mode warm**    put data in redis list

 **-mode read**    read data from redis list 

 **-mode loop**    read data from the end of list and put it back to begin (like FIFO)

 Example:

 ~~~~bash
    ./bin/lt-redis-cli -mode warm
    Finish warm redis!
 ~~~~

