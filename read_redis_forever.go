package main

import (
    "log"
	"time"
	"fmt"
	"os"
	"strconv"
    "github.com/chasex/redis-go-cluster"
)

func loop() {

    cluster, err := redis.NewCluster(
	&redis.Options{
	    StartNodes: startNodes,
	    ConnTimeout: 50 * time.Millisecond,
	    ReadTimeout: 50 * time.Millisecond,
	    WriteTimeout: 50 * time.Millisecond,
	    KeepAlive: 16,
	    AliveTime: 60 * time.Second,
	})

    if err != nil {
	log.Fatalf("redis.New error: %s", err.Error())
	}
	
    chann := make(chan int, kNumOfRoutine)
    for i := 0; i < kNumOfRoutine; i++ {
	go redisReadAndWrite(cluster, i * qty, (i+1)*qty, chann)
    }

    for i := 0; i < kNumOfRoutine; i++ {
	_ = <-chann
    }
}

func redisReadAndWrite(cluster *redis.Cluster, begin, end int, done chan int) {
	
    for {

		// Чтение из кластера
		value, err := cluster.Do("RPOP", listName)
		if err != nil {
			fmt.Printf("-error: %s\n", err.Error())
			time.Sleep(100 * time.Millisecond)
			continue
		}

		if value == nil {
			break
		}

		fmt.Printf("get %s\n", value)
		time.Sleep(500 * time.Millisecond)

		// запись обратно в кластер
		_, err = cluster.Do("LPUSH", listName, value)
		if err != nil {
			fmt.Printf("-set %s\n", err.Error())
			time.Sleep(100 * time.Millisecond)
			continue
		}
    }

    done <- 1
}


// Simple helper function to read an environment or return a default value
func getEnv(key string, defaultVal string) string {
    if value, exists := os.LookupEnv(key); exists {
	return value
    }

    return defaultVal
}

// Simple helper function to read an environment variable into integer or return a default value
func getEnvAsInt(name string, defaultVal int) int {
    valueStr := getEnv(name, "")
    if value, err := strconv.Atoi(valueStr); err == nil {
	return value
    }

    return defaultVal
}
