package main

import (
    "log"
    "time"
	"fmt"
	"strconv"
    "github.com/chasex/redis-go-cluster"
)

func warm() {
    cluster, err := redis.NewCluster(
	&redis.Options{
	    StartNodes: startNodes,
	    ConnTimeout: 50 * time.Millisecond,
	    ReadTimeout: 50 * time.Millisecond,
	    WriteTimeout: 50 * time.Millisecond,
	    KeepAlive: 16,
	    AliveTime: 60 * time.Second,
	})

    if err != nil {
	log.Fatalf("redis.New error: %s", err.Error())
	}
	
    chann := make(chan int, kNumOfRoutine)
    for i := 0; i < kNumOfRoutine; i++ {
	go putInList(cluster, i * qty, (i+1)*qty, chann)
    }

    for i := 0; i < kNumOfRoutine; i++ {
	_ = <-chann
	}
	
	fmt.Println("Finish warm redis!")
}

func putInList(cluster *redis.Cluster, begin, end int, done chan int) {

    for i := begin; i < end; i++ {
		value := listName + strconv.Itoa(i)

		// запись в кластер
		_, err := cluster.Do("LPUSH", listName, value)
		if err != nil {
			fmt.Printf("-set %s: %s\n", value, err.Error())
			time.Sleep(100 * time.Millisecond)
			continue
		}
    }

    done <- 1
}

